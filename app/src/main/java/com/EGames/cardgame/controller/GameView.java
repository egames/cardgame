package com.EGames.cardgame.controller;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.EGames.cardgame.MainActivity;
import com.EGames.cardgame.R;
import com.EGames.cardgame.dao.Connection;
import com.EGames.cardgame.logic.Card;
import com.EGames.cardgame.logic.Game;
import com.EGames.cardgame.logic.Player;
import com.EGames.cardgame.logic.Speed;

import java.util.ArrayList;


/**
 * Created by kerplo on 2/08/14.
 */
public class GameView extends SurfaceView  implements SurfaceHolder.Callback{

    private static final int BMP_ROWS = 4;
    private static final int BMP_COLUMNS = 13;
    private SurfaceHolder holder;
    private GameLoopThread gameLoopThread;
    private Bitmap bmp_background;
    private Bitmap bmp_shadow;
    private Bitmap bmp_shadow_stack;
    private Bitmap bmp_cards;
    public Bitmap bmp_backend;
    private Rect dst_shadow;
    private Rect dst_backgroud;
    private Rect dst_shadow_stack;
    private Game game;
    private GameView gameView;
    private Player player;
    private Card cardSelected = new Card();
    private int posCard;
    private int card_width;
    private int card_height;
    private boolean calculatePos = true;
    private boolean cardMoving = false;
    private String id;
    private Connection connection;
    private static GameView instance;

    private Thread connectToServer;
    private Thread connectToTable;
    private Thread readyToPlay;

    public GameView(final Context context) {
        super(context);
        this.instance = this;
        gameView = this;
        game = new Game();
        gameLoopThread = new GameLoopThread(this);
        holder = getHolder();
        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (gameLoopThread.getState()==Thread.State.TERMINATED) {
            gameLoopThread = new GameLoopThread(gameView);
        }
        gameLoopThread.setRunning(true);
        gameLoopThread.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        gameLoopThread.setRunning(false);
        gameLoopThread.getThreadGroup().interrupt();
        while (retry) {
            try {
                gameLoopThread.join();
                retry = false;
            } catch (InterruptedException e) {
            }
        }
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************          DRAW && TOUCH           ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        if (calculatePos) {
            loadResources();
            createCards(bmp_cards);
            calculatePosiciones(null);
        }
        canvas.drawBitmap(bmp_background, null, dst_backgroud, null);
        canvas.drawBitmap(bmp_shadow, null, dst_shadow, null);
        canvas.drawBitmap(bmp_shadow_stack, null, dst_shadow_stack, null);
        int nCards = 1;
        for (int i = 0; i < game.getDeck().size() && !game.getDeck().isEmpty() && 0 < nCards; ++i) {
            if (game.getDeck().get(i).isDistributed()) {
                continue;
            }
            --nCards;
            game.getDeck().get(i).getCardSprite().onDraw(canvas, game.getDeck().get(i));
        }
        for (int i = 0; i < game.getStack().size() && !game.getStack().isEmpty(); ++i) {
            game.getStack().get(i).getCardSprite().onDraw(canvas, game.getStack().get(i));
        }
        for (Player player : game.getPlayersList()) {
            for (Card card : player.getHandList()) {
                card.getCardSprite().onDraw(canvas, card);
            }
        }
        if (cardMoving) {
            cardSelected.getCardSprite().onDraw(canvas, cardSelected);
        }

        if(null!=player && player.isTurn()) {
            Paint paint = new Paint();
            paint.setColor(Color.BLACK);
            paint.setTextSize(30);
            paint.setTypeface(Typeface.DEFAULT_BOLD);
            canvas.drawText(MainActivity.getInstance().getString(R.string.turn), dst_shadow.right, dst_shadow.top - 10, paint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        synchronized (getHolder()) {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                for (Card card : player.getHandList()) {
                    if (card.getCardSprite().isCollition(event.getX(), event.getY())) {
                        card.setTouched(true);
                        cardSelected = card;
                        posCard = player.getHandList().indexOf(card);
                        player.getHandList().remove(card);
                        cardMoving = true;
                        break;
                    }
                }
            }
            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                if (cardSelected.isTouched()) {
                    cardSelected.setPosx((int) event.getX() - card_width / 2);
                    cardSelected.setPosy((int) event.getY() - card_height / 2);
                }
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (cardSelected.isTouched()) {
                    boolean desplazada = false;
                    for (Card card : player.getHandList()) {
                        if (card.getCardSprite().isCollition(cardSelected.getPosx() + card_width / 2, cardSelected.getPosy() + card_height / 2)) {
                            desplazada = true;
                            if (cardSelected.getPosx() + card_width / 2 > cardSelected.getPosxInitial()) {
                                player.getHandList().add(player.getHandList().indexOf(card) + 1, cardSelected);
                            } else {
                                player.getHandList().add(player.getHandList().indexOf(card), cardSelected);
                            }
                            cardSelected.setTouched(false);
                            cardMoving = false;
                            calculatePosiciones(player);
                            break;
                        }
                    }
                    if ( player.isTurn() && (dst_shadow_stack.contains((int) cardSelected.getPosx() + card_width/2, (int) cardSelected.getPosy() + card_height/2))){
                        connection.leaveCard(Integer.toString(game.getDeck().indexOf(cardSelected)));
                        desplazada = true;
                        cardSelected.setPosxInitial(this.getWidth() / 2 - card_width / 2);
                        cardSelected.setPosyInitial(dst_shadow_stack.top + (dst_shadow_stack.height() - card_height) / 2);
                        cardSelected.setTouched(false);
                        cardSelected.setBackend(false);
                        cardMoving = false;
                        game.getStack().add(cardSelected);
                        cardSelected.setInStack(true);
                        calculatePosiciones(player);
                        calculateMovement(cardSelected);
                    }
                    if ( !desplazada ) {
                        calculateMovement(cardSelected);
                        player.getHandList().add( posCard, cardSelected);
                        cardSelected.setTouched( false );
                        cardMoving = false;
                    }
                }
            }
        }
        return true;
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************          DRAW RESOURCES          ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    private void loadResources() {
        bmp_cards = decodeSampledBitmapFromResource(getResources(), R.drawable.cartas_poker, this.getWidth(), this.getHeight());
        bmp_backend = decodeSampledBitmapFromResource(getResources(), R.drawable.backend_card, this.getWidth(), this.getHeight());
        bmp_background = BitmapFactory.decodeResource(getResources(), R.drawable.mesa2);
        bmp_shadow = decodeSampledBitmapFromResource(getResources(), R.drawable.sombra, this.getWidth(), this.getHeight());
        bmp_shadow_stack = decodeSampledBitmapFromResource(getResources(), R.drawable.sombra_monton, this.getWidth(), this.getHeight());
        // TODO cargar bmp backend carta.
    }

    private void createCards(Bitmap bmp) {
        card_width = bmp.getWidth() / BMP_COLUMNS;
        card_height = bmp.getHeight() / BMP_ROWS;
        int index = 0;

        for (int y=0, suit=0; y<bmp.getHeight(); y+=card_height, ++suit) {
            for(int x=0, value=1; x<bmp.getWidth(); x+=card_width, ++value) {
                // TODO Revisar las resoluciones. En otra resolución que no sea la de mi movil peta.
                game.addCard(game.getDeck(), index, value, game.getSuits().get(suit), new Sprite(this, Bitmap.createBitmap(bmp, x, y, card_width, card_height)));
                index++;
            }
        }
    }

    private void calculatePosiciones(Player player) {
        if (calculatePos) {
            dst_backgroud = new Rect(0, 0, this.getWidth(), this.getHeight());
            dst_shadow = new Rect(this.getWidth() / 2 - bmp_shadow.getWidth() / 3,
                    this.getHeight() - bmp_shadow.getHeight() / 2 - 10, this.getWidth() / 2 + bmp_shadow.getWidth() / 3, this.getHeight() - 10);
            dst_shadow_stack = new Rect(this.getWidth() / 2 - bmp_shadow_stack.getWidth() / 3,
                    this.getHeight() / 2 - bmp_shadow_stack.getHeight() / 3, this.getWidth() / 2 + bmp_shadow_stack.getWidth() / 3, this.getHeight() / 2 + bmp_shadow_stack.getHeight() / 3);
            // TODO Completar lógica para el mazo de cartas.
            for (Card card : game.getDeck()) {
                card.setPosxInitial((int)(this.getWidth() / 2 - card_width * 2.5f));
                card.setPosyInitial(dst_shadow_stack.top + (dst_shadow_stack.height() - card_height) / 2);
                card.setPosx(card.getPosxInitial());
                card.setPosy(card.getPosyInitial());
            }
            if(null==player) {
                for (Player playerAux : game.getPlayersList()) {
                    int desp = 0;
                    for (Card card : playerAux.getHandList()) {
                        if (playerAux.getId().equals(this.id)) {
                            card.setPosxInitial(this.getWidth() / 2 - (card_width + 10) * playerAux.getHandList().size() / 2 + (card_width + 10) * desp);
                            card.setPosyInitial(dst_shadow.top + (dst_shadow.height() - card_height) / 2);
                            card.setPosx(this.getWidth() / 2 - card_width * 13 / 10);
                            card.setPosy((this.getHeight() / 2 - bmp_shadow_stack.getHeight() / 2) + (bmp_shadow_stack.getHeight() - card_height));
                            calculateMovement(card);
                            desp++;
                        } else {
                            card.setPosxInitial(this.getWidth() / 2 - (card_width / 2) * game.getPlayersList().get(1).getHandList().size() / 2 + (card_width / 2) * desp);
                            card.setPosyInitial(-card_width / 3);
                            card.setPosx(this.getWidth() / 2 - card_width * 13 / 10);
                            card.setPosy((this.getHeight() / 2 - bmp_shadow_stack.getHeight() / 2) + (bmp_shadow_stack.getHeight() - card_height));
                            card.setAngle(-150.0f - (desp * (60.0f / game.getPlayersList().get(1).getHandList().size())));
                            calculateMovement(card);
                            desp++;
                        }
                    }
                }
            } else {
                int desp = 0;
                for (Card card : player.getHandList()) {
                    if (player.getId() == id) {
                        card.setPosxInitial(this.getWidth() / 2 - (card_width + 10) * player.getHandList().size() / 2 + (card_width + 10) * desp);
                        card.setPosyInitial(dst_shadow.top + (dst_shadow.height() - card_height) / 2);
                        card.setPosx(this.getWidth() / 2 - card_width * 13 / 10);
                        card.setPosy((this.getHeight() / 2 - bmp_shadow_stack.getHeight() / 2) + (bmp_shadow_stack.getHeight() - card_height));
                        calculateMovement(card);
                        desp++;
                    } else {
                        card.setPosxInitial(this.getWidth() / 2 - (card_width / 2) * game.getPlayersList().get(1).getHandList().size() / 2 + (card_width / 2) * desp);
                        card.setPosyInitial(-card_width / 3);
                        card.setPosx(this.getWidth() / 2 - card_width * 13 / 10);
                        card.setPosy((this.getHeight() / 2 - bmp_shadow_stack.getHeight() / 2) + (bmp_shadow_stack.getHeight() - card_height));
                        card.setAngle(-150.0f - (desp * (60.0f / game.getPlayersList().get(1).getHandList().size())));
                        calculateMovement(card);
                        desp++;
                    }
                }
            }
            calculatePos = false;
        } else {
            int desp = 0;
            for(Player playerAux : game.getPlayersList()) {
                for (Card card : playerAux.getHandList()) {
                    if (playerAux.getId().equals(this.id)) {
                        card.setPosxInitial(this.getWidth() / 2 - (card_width + 10) * player.getHandList().size() / 2 + (card_width + 10) * desp);
                        card.setPosyInitial(dst_shadow.top + (dst_shadow.height() - card_height) / 2);
                        calculateMovement(card);
                        desp++;
                    }
                }
            }
        }
    }

    private void calculateMovement(Card card) {
        float componenteX = card.getPosxInitial() - card.getPosx();
        float componenteY = card.getPosyInitial() - card.getPosy();
        float distancia = (float) Math.sqrt(componenteX * componenteX + componenteY * componenteY);
        card.setSpeed(new Speed(componenteX / distancia, componenteY / distancia));
        card.setGoBack(true);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************               GAME               ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    public void addCards(String playerID, final ArrayList<Integer> cards) {
        Player player = null;
        for (Player playerAux: game.getPlayersList()){
            if(playerAux.getId().equals(playerID)) {
                player = playerAux;
            }
        }
        if(null==player) {
            player = game.addPlayer(playerID);
        }
        for (int i = 0; i < cards.size(); i++) {
            if (!playerID.equals(this.id)) {
                game.deliverCard(player, game.getDeck().get(cards.get(i)), true);
            } else {
                game.deliverCard(player, game.getDeck().get(cards.get(i)), false);
            }
        }
        calculatePos = true;
        calculatePosiciones(null);
    }

    public void leaveCard(int cardID) {
        Card card = game.getDeck().get(cardID);
        if(!game.getStack().contains(card)) {
            card.setPosxInitial(this.getWidth() / 2 - card_width / 2);
            card.setPosyInitial(dst_shadow_stack.top + (dst_shadow_stack.height() - card_height) / 2);
            card.setTouched(false);
            card.setBackend(false);
            card.setInStack(true);
            game.leaveCard(card);
            calculateMovement(card);
        }
    }

    public void changeTurn(boolean turn) {
        game.setTurn(player, turn);
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************          COMMUNICATION           ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    public synchronized void beginCommunicationWithServer() {
        MainActivity.getInstance().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                MainActivity.getInstance().countDownTimer = new CountDownTimer(10000, 1000) {
                    public void onTick(long millisUntilFinished) {
                    }
                    public void onFinish() {
                        connectToServer.interrupt();
                        connectToTable.interrupt();
                        readyToPlay.interrupt();
                        MainActivity.getInstance().progressDialog.dismiss();
                        MainActivity.getInstance().showDialogConnectionProblem();
                    }
                }.start();
            }
        });
        startThreads();
        connectToServer.start();
        connectToTable.start();
        readyToPlay.start();
    }

    public void startThreads() {
        connection.setConnectedToServer(false);
        connection.setConnectedToTable(false);
        connection.setReadyToPlay(false);
        connectToServer = new Thread(new
                                             Runnable() {
                                                 @Override
                                                 public void run() {
                                                     connection.connectToServer(id);
                                                     MainActivity.getInstance().runOnUiThread(new Runnable() {

                                                         @Override
                                                         public void run() {
                                                             if(null!=MainActivity.getInstance().progressDialog) {
                                                                 MainActivity.getInstance().setMessageProgressDialog(
                                                                         MainActivity.getInstance().getString(
                                                                                 R.string.connecting_to_server));
                                                             }
                                                         }
                                                     });
                                                 }
                                             });
        connectToTable = new Thread(new
                                            Runnable() {
                                                @Override
                                                public void run() {
                                                    connection.connectToTable();
                                                    MainActivity.getInstance().runOnUiThread(new Runnable() {

                                                        @Override
                                                        public void run() {
                                                            if(null!=MainActivity.getInstance().progressDialog) {
                                                                MainActivity.getInstance().setMessageProgressDialog(
                                                                        MainActivity.getInstance().getString(
                                                                                R.string.connecting_to_players));
                                                            }
                                                        }
                                                    });
                                                }
                                            });
        readyToPlay = new Thread(new
                                         Runnable() {
                                             @Override
                                             public void run() {
                                                 connection.readyToPlay();
                                                 if(null!=MainActivity.getInstance().progressDialog) {
                                                     MainActivity.getInstance().progressDialog.dismiss();
                                                 }
                                                 MainActivity.getInstance().countDownTimer.cancel();
                                             }
                                         });
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************         GETTER && SETTER         ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    public void setId(String id) {
        this.id = id;
        this.player = game.addPlayer(id);
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public static GameView getInstance() {
        return instance;
    }
}