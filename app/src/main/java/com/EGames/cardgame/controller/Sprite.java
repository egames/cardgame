package com.EGames.cardgame.controller;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.EGames.cardgame.logic.Card;

import java.util.Random;

/**
 * Created by kerplo on 2/08/14.
 */
public class Sprite {
    private GameView gameView;
    private Bitmap bmp;
    private Card card;
    private float x;
    private float y;
    private int width;
    private int height;
    private float angle;
    private Paint paint;

    public Sprite(GameView gameView, Bitmap bmp) {
        this.gameView = gameView;
        this.bmp = bmp;
        this.width = bmp.getWidth();
        this.height = bmp.getHeight();
        this.angle = randomAngle();
        paint = new Paint(Paint.ANTI_ALIAS_FLAG
                | Paint.FILTER_BITMAP_FLAG);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);
    }

    private void update() {
        card.volverPosInicial();
    }

    public void onDraw(Canvas canvas, Card card) {
        this.card = card;
        update();
        this.x = card.getPosx();
        this.y = card.getPosy();
        Rect dst = new Rect((int)x, (int)y, (int)x + width, (int)y + height);
        canvas.save();
        if (card.getAngle()!=0.0f)
            canvas.rotate(card.getAngle(), x + width/2, y + height/2);
        if (card.isInStack())
            canvas.rotate(angle, x + width/2, y + height/2);
        if (card.isBackend())
            canvas.drawBitmap(gameView.bmp_backend, null, dst, paint);
        else
            canvas.drawBitmap(bmp, null, dst, paint);
        canvas.restore();
    }

//    public void girarCarta (Canvas canvas, Bitmap backend) {
//        this.x = carta.getPosx();
//        this.y = carta.getPosy();
//
//        Matrix m = new Matrix();
//        m.preScale(-1, 1);
//        Bitmap dst = Bitmap.createBitmap(bmp, (int) x, (int) y, (int) x + width / 2, (int) y + height / 2, m, false);
//        dst.setDensity(DisplayMetrics.DENSITY_DEFAULT);
//        canvas.drawBitmap(bmp, null, null);
//    }


    public Bitmap getBmp() {
        return bmp;
    }

    private float randomAngle () {
        float minX = -45.0f;
        float maxX = 45.0f;
        Random rand = new Random();
        return rand.nextFloat() * (maxX - minX) + minX;
    }

    public boolean isCollition(float x2, float y2) {
        return x2 > x && x2 < x + width && y2 > y && y2 < y + height;
    }

//    public void rotarImagen(Bitmap source, float angle)
//    {
//        Matrix matrix = new Matrix();
//        matrix.postRotate(angle, x+source.getWidth(), y+source.getHeight());
//        matrix.postTranslate(source.getHeight(), 0);
//        this.bmp = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
//        matrix.reset();
////        this.bmp = Bitmap.createBitmap(source, (int)x, (int)y, (int)x + width/2, (int)y + height/2, matrix, true);
//    }
}