package com.EGames.cardgame.constants;

/**
 * Created by kerplo on 29/07/14.
 */
public class Constants {
    // BARAJAS
    public static final int BARAJA_ESPAÑOLA = 0;
    public static final int BARAJA_POKER = 1;

    // PALOS
    public static final int ORO = 0;
    public static final int ESPADA = 1;
    public static final int COPA = 2;
    public static final int BASTO = 3;

    // VARIABLES
    public static final int VALOR_MAX = 12;
    public static final int PALO_MAX = 4;

    public static final int VECES_BARAJAR = 2;
    public static final int NUMERO_CARTAS = 47;
}
