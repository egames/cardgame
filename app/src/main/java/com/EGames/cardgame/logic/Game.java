package com.EGames.cardgame.logic;

import com.EGames.cardgame.constants.Constants;
import com.EGames.cardgame.controller.Sprite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by kerplo on 2/08/14.
 */
public class Game {
    private List<String> suits;
    private List<Card> deck;
    private List<Card> stack;
    private List<Player> playersList;
    private Player player;

    public Game() {
        // Heart, Club (Trebol), Spade, Diamond
        suits = new ArrayList<>(Arrays.asList("H", "C", "S", "D"));
        deck = Collections.synchronizedList(new ArrayList<Card>());
        stack = Collections.synchronizedList(new ArrayList<Card>());
        playersList = Collections.synchronizedList(new ArrayList<Player>());
    }

    public Player addPlayer(String id) {
        Player player = new Player(id);
        playersList.add(player);
        return player;
    }

    public void addCard(List<Card> list, int index, int value, String suit, Sprite cartaSprite) {
        list.add(new Card(index, value, suit, cartaSprite));
    }

    public void shuffleCards(List<Card> list) {
        Card card;
        int pos_nueva;
        for(int veces=0; veces< Constants.VECES_BARAJAR; ++veces)
            for(int pos_vieja=0; pos_vieja<list.size(); ++pos_vieja){
                pos_nueva = randInt(0,list.size()-1);
                card = list.get(pos_vieja);
                list.set(pos_vieja, list.get(pos_nueva));
                list.set(pos_nueva, card);
            }
    }

    public Card deliverCard(Player player, Card card, boolean backend) {
        card.setBackend(backend);
        card.setDistributed(true);
        player.getHandList().add(card);
        return card;
    }

    public void setTurn(Player player, boolean turn) {
        player.setTurn(turn);
    }

    public void pickUp(List<Card> list, Card card) {
        for(int posicion=0; posicion<list.size(); ++posicion){
            card = list.get(posicion);
            card.setDistributed(false);
        }
    }

    public void leaveCard(Card card) {
        stack.add(card);
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************         GETTER && SETTER         ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    public List<Player> getPlayersList() {
        return playersList;
    }

    public void setPlayersList(ArrayList<Player> playersList) {
        this.playersList = playersList;
    }

    public List<Card> getDeck() {
        return deck;
    }

    public void setDeck(List<Card> deck) {
        this.deck = deck;
    }

    public List<Card> getStack() {
        return stack;
    }

    public void setStack(List<Card> stack) {
        this.stack = stack;
    }

    public List<String> getSuits() {
        return suits;
    }

    public void setSuits(List<String> suits) {
        this.suits = suits;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
