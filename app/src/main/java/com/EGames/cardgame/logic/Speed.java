package com.EGames.cardgame.logic;

/**
 * Created by kerplo on 30/08/14.
 */
public class Speed {
    private float xv = 1;	// velocity value on the X axis
    private float yv = 1;	// velocity value on the Y axis

    public Speed(float xv, float yv) {
        this.xv = xv;
        this.yv = yv;
    }

    public float getXv() {
        return xv;
    }
    public void setXv(float xv) {
        this.xv = xv;
    }
    public float getYv() {
        return yv;
    }
    public void setYv(float yv) {
        this.yv = yv;
    }
}
