package com.EGames.cardgame.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kerplo on 2/08/14.
 */
public class Player {
    private String id;
    private boolean turn;
    private List<Card> handList;

    public Player(String id) {
        handList = Collections.synchronizedList(new ArrayList<Card>());
        this.id = id;
        this.turn = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Card> getHandList() {
        return handList;
    }

    public void setHandList(List<Card> handList) {
        this.handList = handList;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }
}
