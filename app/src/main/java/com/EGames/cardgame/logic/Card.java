package com.EGames.cardgame.logic;

import android.graphics.Canvas;

import com.EGames.cardgame.controller.Sprite;

/**
 * Created by kerplo on 29/07/14.
 */
public class Card {
    private int index;
    private int value;
    private String suit;
    public int posxInitial;
    public int posyInitial;
    private float posx;
    private float posy;
    private float angle;
    private Speed speed;
    private boolean distributed;
    private boolean touched;
    private boolean goBack;
    private boolean inStack;
    private boolean backend;
    private Sprite cardSprite;
    public Canvas canvas;

    public Card(){}

    public Card(int index, int value, String suit, Sprite cardSprite) {
        this.index = index;
        this.value = value;
        this.suit = suit;
        this.speed = new Speed(0, 0);
        this.cardSprite = cardSprite;
        this.distributed = false;
        this.goBack = false;
        this.inStack = false;
        this.backend = true;
        this.canvas = new Canvas();
        this.angle = 0.0f;
    }

    public int getIndex() {
        return index;
    }

    public int getPosxInitial() {
        return posxInitial;
    }

    public void setPosxInitial(int posxInitial) {
        this.posxInitial = posxInitial;
    }

    public int getPosyInitial() {
        return posyInitial;
    }

    public void setPosyInitial(int posyInitial) {
        this.posyInitial = posyInitial;
    }

    public float getPosx() {
        return posx;
    }

    public void setPosx(int posx) {
        this.posx = posx;
    }

    public float getPosy() {
        return posy;
    }

    public void setPosy(int posy) {
        this.posy = posy;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public Speed getSpeed() {
        return speed;
    }

    public void setSpeed(Speed speed) {
        this.speed = speed;
    }

    public boolean isDistributed() {
        return distributed;
    }

    public void setDistributed(boolean distributed) {
        this.distributed = distributed;
    }

    public boolean isBackend() {
        return backend;
    }

    public void setBackend(boolean backend) {
        this.backend = backend;
    }

    public Sprite getCardSprite() {
        return cardSprite;
    }

    public boolean isTouched() {
        return touched;
    }

    public void setTouched(boolean touched) {
        this.touched = touched;
    }

    public void setGoBack(boolean goBack) {
        this.goBack = goBack;
    }

    public boolean isInStack() {
        return inStack;
    }

    public void setInStack(boolean inStack) {
        this.inStack = inStack;
    }

    public void volverPosInicial() {
        if(goBack) {
            if((int)posx - posxInitial <= 15 && (int)posx - posxInitial >= -15 && (int)posy - posyInitial <= 15 && (int)posy - posyInitial >= -15) {
                goBack = false;
                posx = posxInitial;
                posy = posyInitial;
            } else {

                posx += (speed.getXv() * 30);
                posy += (speed.getYv() * 30);
            }
        }
    }
}
