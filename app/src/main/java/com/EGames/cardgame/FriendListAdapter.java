package com.EGames.cardgame;

import android.app.Service;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class FriendListAdapter extends ArrayAdapter<String> {

    private Context context;
    public ArrayList<String> data;

    public FriendListAdapter(Context context, int resource, ArrayList<String> data) {
        super(context,resource,data);

        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Service.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.friends_list_item,null);
        }

        String friend = data.get(position);

        TextView player_text = (TextView) convertView.findViewById(R.id.player_text);
        player_text.setText(friend);

        return convertView;
    }

    public void setData(ArrayList data) {
        this.data = data;
    }
    
}