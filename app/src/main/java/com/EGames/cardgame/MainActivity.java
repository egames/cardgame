package com.EGames.cardgame;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.EGames.cardgame.controller.GameView;
import com.EGames.cardgame.dao.Connection;
import com.EGames.cardgame.dao.LoadImage;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.plus.Plus;
import com.google.example.games.basegameutils.BaseGameUtils;

import java.util.ArrayList;

/**
 * Created by kerplo on 02/10/14.
 */
public class MainActivity extends Activity
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener {

    private static final String TAG = "CardGames";

    // Request code used to invoke sign in user interactions.
    private static final int RC_SIGN_IN = 9001;

    // Client used to interact with Google APIs.
    private GoogleApiClient mClient;

    // Are we currently resolving a connection failure?
    private boolean mResolvingConnectionFailure = false;

    // Has the user clicked the sign-in button?
    private boolean mSignInClicked = false;

    // Set to true to automatically start the sign in flow when the Activity starts.
    // Set to false to require the user to click the button in order to sign in.
    private boolean mAutoStartSignInFlow = true;

    private Connection connection;

    private AlertDialog mAlertDialog;

    private String namePlayer;

    private Bitmap imagePlayer;

    private String idGame;

    private static MainActivity instance;

    public ProgressDialog progressDialog;

    public CountDownTimer countDownTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.instance = this;
        // create an instance of Google API client and specify the Play services
        // and scopes to use. In this example, we specify that the app wants
        // access to the Games, and Plus services and scopes.
        // Create the Google API Client with access to Plus and Games

        mClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API).addScope(Plus.SCOPE_PLUS_LOGIN)
                .addApi(Games.API).addScope(Games.SCOPE_GAMES)
                .build();

        findViewById(R.id.sign_out_button).setOnClickListener(this);
        findViewById(R.id.sign_in_button).setOnClickListener(this);
        showSpinner();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart()");
        super.onStart();
        mClient.connect();
        connection = new Connection();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop()");
        if (mClient.isConnected()) {
            mClient.disconnect();
        }
        connection.disconnectServer();
        super.onStop();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus && android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);}
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************        CONNECTION METHODS        ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConnected() called. Sign in successful!");
        this.getCurrentFocus();
        showSignOutBar();
        idGame = Games.Players.getCurrentPlayer(mClient).getPlayerId();
        namePlayer = Games.Players.getCurrentPlayer(mClient).getDisplayName();
        if(Games.Players.getCurrentPlayer(mClient).hasIconImage()) {
            LoadImage loadImage = new LoadImage();
            loadImage.setImg(((ImageView) findViewById(R.id.profile_image)));
            loadImage.execute(Games.Players.getCurrentPlayer(mClient).getIconImageUrl());
        }
        showUserInfo();
        dismissSpinner();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "onConnectionSuspended() called. Trying to reconnect.");
        mClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed() called, result: " + connectionResult);

        if (mResolvingConnectionFailure) {
            Log.d(TAG, "onConnectionFailed() ignoring connection failure; already resolving.");
            return;
        }

        if (mSignInClicked || mAutoStartSignInFlow) {
            mAutoStartSignInFlow = false;
            mSignInClicked = false;
            mResolvingConnectionFailure = BaseGameUtils.resolveConnectionFailure(this, mClient,
                    connectionResult, RC_SIGN_IN, getString(R.string.signin_other_error));
        }
        showSignInBar();
    }

    private void showUserInfo() {
        if(null!=imagePlayer) {
            ((ImageView) findViewById(R.id.profile_image)).setImageBitmap(imagePlayer);
        }
        ((TextView) findViewById(R.id.name_field)).setText(namePlayer);
        findViewById(R.id.profile_image).setVisibility(View.VISIBLE);
    }

    // Shows the "sign in" bar (explanation and button).
    private void showSignInBar() {
        Log.d(TAG, "Showing sign in bar");
//        findViewById(R.id.profile_image).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.name_field)).setText("");
        findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
        findViewById(R.id.sign_out_button).setVisibility(View.GONE);
    }

    // Shows the "sign out" bar (explanation and button).
    private void showSignOutBar() {
        Log.d(TAG, "Showing sign out bar");
        findViewById(R.id.sign_in_button).setVisibility(View.GONE);
        findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************           CLICKS METHODS         ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                Log.d(TAG, "Sign-in button clicked");
                mSignInClicked = true;
                mClient.connect();
                break;
            case R.id.sign_out_button:
                // sign out.
                Log.d(TAG, "Sign-out button clicked");
                mSignInClicked = false;
                Games.signOut(mClient);
                mClient.disconnect();
                showSignInBar();
                break;
        }
    }

    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            Log.d(TAG, "onActivityResult with requestCode == RC_SIGN_IN, responseCode="
                    + responseCode + ", intent=" + intent);
            mSignInClicked = false;
            mResolvingConnectionFailure = false;
            if (responseCode == RESULT_OK) {
                mClient.connect();
            } else {
                BaseGameUtils.showActivityResultError(this,requestCode,responseCode,
                        R.string.signin_failure, R.string.signin_other_error);
            }
        }
    }

    public void onPlayClicked(View view) {
        setContentView(new GameView(this));
        GameView.getInstance().setId(idGame);
        GameView.getInstance().setConnection(connection);
        showProgressDialog();
        GameView.getInstance().beginCommunicationWithServer();
    }

    public void onViewFriends(View view) {
        showSpinner();
        findViewById(R.id.matchup_layout).setVisibility(View.GONE);
        findViewById(R.id.friends_layout).setVisibility(View.VISIBLE);
    }

    public void afterGetFriends(ArrayList list_friends) {
        FriendListAdapter friendsListAdapter;
        GridView friendsList;
        friendsListAdapter = new FriendListAdapter(MainActivity.this, R.layout.friends_list_item, list_friends);
        friendsList = (GridView) findViewById(R.id.listView);
        friendsList.setAdapter(friendsListAdapter);
        dismissSpinner();
    }

    public void onViewMenu(View view) {
        findViewById(R.id.friends_layout).setVisibility(View.GONE);
        findViewById(R.id.matchup_layout).setVisibility(View.VISIBLE);
    }

    /***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************
     **************************            UTIL METHODS          ***********************************
     ***********************************************************************************************
     ***********************************************************************************************
     ***********************************************************************************************/

    public void showSpinner() {
        findViewById(R.id.progressLayout).setVisibility(View.VISIBLE);
    }

    public void dismissSpinner() {
        findViewById(R.id.progressLayout).setVisibility(View.GONE);
    }

    public void showDialogConnectionProblem() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(R.string.server_not_reply).setMessage(R.string.do_you_want_reconnect);
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton(R.string.reconnect,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                showProgressDialog();
                                connection.disconnectServer();
                                GameView.getInstance().beginCommunicationWithServer();
                            }
                        })
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                GameView.getInstance().destroyDrawingCache();
                                setContentView(R.layout.activity_main);
                                showUserInfo();
                            }
                        });
        mAlertDialog = alertDialogBuilder.create();
        mAlertDialog.show();
    }

    public ProgressDialog showProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setIndeterminate(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        return progressDialog;
    }

    public void setMessageProgressDialog (String message) {
        progressDialog.setMessage(message);
    }

    public String getId() {
        return idGame;
    }

    public static MainActivity getInstance() {
        return instance;
    }

    public void setImagePlayer(Bitmap imagePlayer) {
        this.imagePlayer = imagePlayer;
    }
}
