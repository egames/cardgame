package com.EGames.cardgame.dao;

import com.EGames.cardgame.MainActivity;
import com.EGames.cardgame.controller.GameView;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Created by kerplo on 15/10/14.
 */
public class Connection {

    //Socket
    private Socket socket;
    private String name;
    public ArrayList<String> list_friends = new ArrayList<String>();
    private String tableID;
    private boolean connectedToServer = false;
    private boolean connectedToTable = false;
    private boolean readyToPlay = false;
    private static Connection instance;

    public Connection() {
        this.instance = this;
        try{
            System.out.println("Initializing Connection.");

            socket = IO.socket("http://emiliolopezsantiago.com:8080/");
//            socket = IO.socket("http://192.168.2.100:8080/");
            socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {}

            }).on("event", new Emitter.Listener() {

                @Override
                public void call(Object... args) {
                }

            }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {

                @Override
                public void call(Object... args) {}

            });
            socket.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }


    public void connectToServer(String id) {
        this.name = id;
        try {
            System.out.println("Connecting to server.");
            JSONObject object = new JSONObject();
            object.put("name", name);
            socket.emit("connectToServer", object);

            socket.on("logging", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                    connectedToServer = true;
                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//        addFriend();
    }

    public void connectToTable() {
        while(!connectedToServer){
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            System.out.println("Connecting to table.");
            JSONObject object = new JSONObject();
            socket.emit("connectToTable", object);

            socket.on("connectedToTable", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                    try {
                        tableID = obj.get("message").toString();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    connectedToTable = true;
                }
            });
            socket.on("waitingPlayers", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                }
            });
            socket.on("readyToPlay", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                    readyToPlay = true;
                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void readyToPlay() {
        while(!(connectedToTable && readyToPlay)){
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            System.out.println("Ready to play.");
            JSONObject object = new JSONObject();
            object.put("tableID", tableID);
            socket.emit("readyToPlay", object);

            socket.on("play", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                    try {
                        String player = (obj).get("player").toString();
                        JSONArray cards = ((JSONArray) (obj).get("hand"));
                        if(cards.length()>0){
                            ArrayList card_list = new ArrayList();
                            for(int i=0; i<cards.length(); i++){
                                card_list.add(Integer.valueOf(cards.get(i).toString()));
                            }
                            GameView.getInstance().addCards(player, card_list);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            socket.on("turn", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                    try {
                        GameView.getInstance().changeTurn((boolean) (obj).get("turn"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void leaveCard(String card) {
        try {
            System.out.println("Leaving card.");
            JSONObject object = new JSONObject();
            object.put("tableID", tableID);
            object.put("card", card);
            socket.emit("leaveCard", object);

            socket.on("ilegalMovement", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);

                }
            });

            socket.on("leavingCard", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    JSONObject obj = (JSONObject)args[0];
                    System.out.println("Server said: " + obj);
                    try {
                        GameView.getInstance().leaveCard(Integer.parseInt(obj.get("card").toString()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void changeTurn() {
        socket.on("turn", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject obj = (JSONObject)args[0];
                System.out.println("Server said: " + obj);
                try {
                    GameView.getInstance().changeTurn((boolean) (obj).get("turn"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getCard() {
        try {
            System.out.println("Getting card.");
            JSONObject object = new JSONObject();
            object.put("tableID", "1");
            socket.emit("getCard", object);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void addFriend() {
        try {
            System.out.println("Adding a friend.");
            JSONObject object = new JSONObject();
            object.put("name", name);
            object.put("friend", "jose");
            socket.emit("addFriend", object);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void getFriends() {
        try {
            System.out.println("Getting friends.");
            JSONObject object = new JSONObject();
            object.put("name", name);
            socket.emit("getFriends", object);

            socket.on("gettingFriends", new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        list_friends.clear();
                        for(int i=0; i<((JSONArray)args[0]).length(); i++) {
                            list_friends.add((String) ((JSONObject) ((JSONArray) args[0]).get(i)).get("friend"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MainActivity.getInstance().afterGetFriends(list_friends);
                }
            });
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void disconnectServer() {
        try {
            System.out.println("Disconnectig the server.");
            JSONObject object = new JSONObject();
            object.put("name", name);
            object.put("tableID", "1");
            socket.emit("disconnect", object);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setConnectedToServer(boolean connectedToServer) {
        this.connectedToServer = connectedToServer;
    }

    public void setConnectedToTable(boolean connectedToTable) {
        this.connectedToTable = connectedToTable;
    }

    public void setReadyToPlay(boolean readyToPlay) {
        this.readyToPlay = readyToPlay;
    }

    public static Connection getInstance() {
        return instance;
    }
}
