package com.EGames.cardgame.dao;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.EGames.cardgame.MainActivity;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by kerplo on 17/11/14.
 */
public class LoadImage extends AsyncTask<String, String, Bitmap> {
    ImageView img;
    Bitmap bitmap;
    public void setImg(ImageView img) {
        this.img = img;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }
    protected Bitmap doInBackground(String... args) {
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(args[0]).getContent());
            MainActivity.getInstance().setImagePlayer(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }
    protected void onPostExecute(Bitmap image) {
        if(image != null){
            img.setImageBitmap(image);

        }else{

        }
    }
}